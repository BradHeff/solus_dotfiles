# Solus i3 dotfiles

This is my Solus i3 setup

REQUIREMENTS
---

requirements for the polybar scripts and i3 config scripts

* rofi
* perl
	* Modules
		* Capture::Tiny
		* JSON (for reddit module)
		* XML::LibXML

To install perl modules enter
	
	cpan

while in cpan console enter

	install Capture::Tiny
	install JSON
	install XML::LibXML


PROGRAMS USED IN SCREENSHOTS
---

* urxvt
* polybar
* rofi
* NetworkManager
* i3-gaps [gaps-next (fork)](https://github.com/max-lv/i3)
* iftop
* vtop [vtop](https://github.com/MrRio/vtop)
* weechat
* cava [cava](https://github.com/karlstav/cava)
* ncmpcpp

THEMES USED
---

* Arc-Orange-Dark GTK Theme [Arc-Orange_Dark](https://github.com/eti0/arc-theme-orange)
* Paper Icon Theme [paper-icon-theme](https://github.com/snwh/paper-icon-theme)


CLONE REPOSITORY
---

to clone this repository enter

	git clone https://gitlab.com/BradHeff/solus_dotfiles.git && cd solus_dotfiles


![](Screenshots/screenshot1.png)

![](Screenshots/screenshot2.png)

![](Screenshots/screenshot3.png)

![](Screenshots/screenshot4.png)

![](Screenshots/screenshot5.png)

![](Screenshots/screenshot6.png)

![](Screenshots/screenshot7.png)

![](Screenshots/LockScreen.png)