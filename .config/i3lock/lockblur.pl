#!/usr/bin/env perl

use strict;
use warnings;
use Image::Magick;
#use Time::Piece;

my $Base_DIR = "/tmp/Lock";
my $lock = "/home/pheonix/.config/i3lock/lock6.png";

sub checks {
	if(! -d $Base_DIR){ 
		unless(mkdir $Base_DIR) {
        	warn "Unable to create $Base_DIR\n";
        	return 0;        	
    	}
	}
	return 1;
}

sub blur {
	my $image = Image::Magick->new;
	$image->Read($_[0]);

	$image->Blur("0x5");
	$image->Set(quality=>100);
	$image->Write($_[0]);
		
	undef $image;
}

sub add_lock {
	my $image = Image::Magick->new;
	$image->Read($_[0]);

	my $logo1 = Image::Magick->new;
	$logo1->Read("$_[1]");

	my $logo2 = Image::Magick->new;
	$logo2->Read("$_[1]");

	$logo1->Evaluate(
  		operator => 'Multiply',
  		value    => 0.6,
  		channel  => 'Alpha',
	);

	$logo2->Evaluate(
  		operator => 'Multiply',
  		value    => 0.6,
  		channel  => 'Alpha',
	);

	$image->Composite(
  		image    => $logo1,
  		x 		 => 760,
  		y 		 =>	0,
	);

	$image->Composite(
  		image    => $logo2,
  		x 		 => 2680,
  		y 		 =>	0,
	);

	$image->Set(quality=>100);
	$image->Write($_[0]);
	
	undef $image;
	undef $logo1;
	undef $logo2;
}

sub screenlock {
	system qq{i3lock -e -u -i $_[0]};
	system qq{xset dpms force off};
}

sub get_runtime {
	my $parsed = Time::Piece->new();	
	my $diff = $parsed-$_[0];

	return $diff->seconds;
}

sub run {
	if(checks) {
		#my $now = Time::Piece->new();				
		my $basescreen="$Base_DIR/IMG.png";

		system qq{DISPLAY=:0 scrot -q 100 -e 'mv \$f $basescreen'};
		
		blur($basescreen);
		add_lock($basescreen, $lock);
		
		#printf ("lockscreen generated in %s seconds\n", get_runtime($now));
		screenlock($basescreen);
	}
}

run;
exit;