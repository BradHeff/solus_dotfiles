#!/usr/bin/env perl

use strict;
use warnings;
use Capture::Tiny qw(capture);

my $title = "Update";
my $color_normal = "argb:0023262f, argb:F2e0b053, argb:0023262f, argb:F2e0b053, argb:F223262f";
my $color_window = "argb:D923262f, argb:F29e5630, argb:F29e5630";
my $options = "-width -30 -location 5 -bw 1 -dmenu -i -p \"$title\" -lines 5 -color-window \'$color_window\' -color-normal \'$color_normal\' -font \'San Francisco Display Light 10\'";

sub get_item {
	my $list = "Update Repo
Upgrade
Check
Rebuild Database
Remove Orphans";

	my $var = capture { system qq{echo \"$list\" | sort | rofi $options}};
	chomp $var;

	return $var;
}

sub handle_item {
	my $selection = $_[0];
	#my $int = firstidx { $_ eq $selection } @menu;	
	#my $item = $menu[$int];

	chomp($selection);

	if($selection eq "Update Repo"){
		system qq{urxvt -title bash -e sudo eopkg ur};
	}
	elsif($selection eq "Upgrade"){
		system qq{urxvt -title bash -e sudo eopkg up};
	}
	elsif($selection eq "Check"){
		system qq{urxvt -title bash -e sudo eopkg check};
	}
	elsif($selection eq "Remove Orphans"){
		system qq{urxvt -title bash -e sudo eopkg rmo};
	}
	elsif($selection eq "Rebuild Database"){
		system qq{urxvt -title bash -e sudo eopkg rbd};
	}
}

handle_item(get_item);