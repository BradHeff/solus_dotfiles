#!/usr/bin/env perl

use strict;
use warnings;
use Capture::Tiny qw(capture);

my $color;

sub get_updates {
	my $update;
	my $count = 0;
	$update = capture { system qq{eopkg lu} };
	chomp($update);
	#$update = "modem-manager - GNOME Modem Manager";
	my @ups = split("\n", $update);
	
	for (my $var = 0; $var < scalar(@ups); $var++) {
		if($ups[$var] =~ /No packages/){
			last;
		}
		$count += 1;
	}

	if($count == 0){
		$color = '%{F#E0B053} ';
	}
	else {
		$color = '%{F#d7521a} ';
	}
	
	return $color . $count . '%{F-}';
}

print get_updates . "\n";