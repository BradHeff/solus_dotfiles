#!/usr/bin/env perl

use strict;
#use warnings;
use Capture::Tiny qw(capture);
use JSON::XS qw(decode_json);

sub fetch_URI {
	my $url = capture { system qq{curl -s \"https://www.reddit.com/message/unread/.json?feed=066a1bbe91ccabb58085ba33fd90de67b6ef91fd&user=bradheff\"}};
	$url = '[' . $url . ']';
	
	return $url;
}

sub extract_URI {
	my $str = "";
	my $js = decode_json $_[0];

	if($_[0] =~ /error\":/){
		foreach my $x (@$js) {
			$str = $x->{message};						
		}
		return $str;
	}

	foreach my $x (@$js) {
		$str = $x->{data}{dist};		
		if(length $str lt 1){ $str = "0"; }
	}
	
	chomp($str);		
	return $str;	
}

my $msg = extract_URI(fetch_URI);

while ($msg =~ /Requests/i) {
	$msg = extract_URI(fetch_URI);
}

if($msg eq "0" || $msg eq ""){
	print '%{F#E0B053} 0%{F-}' . "\n";
} else {
	print '%{F#d7521a} ' . $msg .'%{F-}' . "\n";
}