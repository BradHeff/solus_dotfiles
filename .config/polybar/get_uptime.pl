#!/usr/bin/env perl

use strict;
use warnings;
use feature qw(say);
use Capture::Tiny qw(capture);

my $uptime;
my $day;
my $hours;
my $minutes;
my $seconds;

sub get_core_uptime {
	$uptime = capture { system qq{cat /proc/uptime | awk '{print \$1}'} };
	$uptime =~ /(\d*)/;
	return $uptime;
}

sub convert_uptime {
	$day = int($_[0]/86400);
	$hours = int(($_[0]%86400)/3600);
	$minutes = int(($_[0]%3600)/60);
	$seconds = $_[0]%60;	
}

sub check_uptime {	
	convert_uptime(get_core_uptime);
	
	$uptime = "";

	if($day ne "0") {
		$uptime = $day . "d";
	}

	if($hours ne "0") {
		$uptime = $uptime . " " . $hours . "h";
	} 
	else { 
		$uptime = $uptime . ""; 
	}

	if($minutes ne "0") {
		$uptime = $uptime . " " . $minutes . "m";	
	} 
	else { 
		$uptime = $uptime . ""; 
	}
		
	$uptime =~ s/^\s//;

	return $uptime;
}

say " " . check_uptime;