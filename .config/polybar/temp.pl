#!/usr/bin/env perl

use strict;
use warnings;
use feature qw(say);
use Capture::Tiny qw(capture);

sub get_temp {
	my $val = capture { system qq{sensors | grep temp1 | grep crit | awk '{print \$2}'}};
	$val =~ s/\++//g;
	$val =~ s/\s+//g;
	return $val;
}

say get_temp;